![Semantic description of Gif](./img/car.gif "Go Car")

<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" >
    <source src="../img/car1.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->



## About the project

This is an integrated project of the Digital House course, in which we will implement all our  "full stack" knowledge (Frontend, Backend, Infrastructure, Testing, Design UI\UX, Database). The goal is build a renting website, in which we choose the car as our product.

## Files about our instructions of each sprint

- [ ] [Figma](https://www.figma.com/file/aua1WHXn4AEwU0r5rxftE0/SPRINT-1-FINAL?node-id=0%3A1) 
- [ ] [1° Sprint](https://docs.google.com/presentation/d/1RvjFpvTDv_BKA06wULQCdXIlZX_aQXhoeJ53rJue1_0/edit#slide=id.g14ba7ce910e_1_1377)



